package model;

import javax.persistence.*;

@Entity
@Table(name = "tests")
public class Test {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id")
//    TODO второй ключ так ради забавы
   // @JoinColumn(name ="device_title")
    private Device device;
    public void setDevice(Device device) {
        this.device = device;
    }
    public Device getDevice(){
        return device;
    }

    @Column(name =  "minTemperature")
    private float minTemperature;
    public void setMinTemperature(float minTemperature){
        this.minTemperature = minTemperature;
    }
    public float getMinTemperature(){
        return minTemperature;
    }
    @Column(name =  "maxTemperature")
    private float maxTemperature;
    public void setMaxTemperature(float maxTemperature){
        this.maxTemperature = maxTemperature;
    }
    public float getMaxTemperature(){
        return maxTemperature;
    }
    @Column(name =  "avrTemperature")
    private float avrTemperature;
    public void setAvrTemperature(float avrTemperature){
        this.avrTemperature = avrTemperature;
    }
    public float getAvrTemperature(){
        return avrTemperature;
    }
    @Column(name =  "minHumidity")
    private float minHumidity;
    public void setMinHumidity(float minHumidity){
        this.minHumidity = minHumidity;
    }
    public float getMinHumidity(){
        return minHumidity;
    }
    @Column(name =  "maxHumidity")
    private float maxHumidity;
    public void setMaxHumidity(float maxHumidity){
        this.maxHumidity = maxHumidity;
    }
    public float getMaxHumidity(){
        return maxHumidity;
    }
    @Column(name =  "avrHumidity")
    private float avrHumidity;
    public void setAvrHumidity(float avrHumidity){
        this.avrTemperature = avrHumidity;
    }
    public float getAvrHumidity(){
        return avrHumidity;
    }
    @Column(name =  "minPressure")
    private float minPressure;
    public void setMinPressure(float minPressure){
        this.minPressure = minPressure;
    }
    public float getMinPressure(){
        return minPressure;
    }
    @Column(name =  "maxPressure")
    private float maxPressure;
    public void setMaxPressure(float maxPressure){
        this.maxPressure = maxPressure;
    }
    public float getMaxPressure(){
        return maxPressure;
    }
    @Column(name =  "avrPressure")
    private float avrPressure;
    public void setAvrPressure(float avrPressure){
        this.avrTemperature = avrPressure;
    }
    public float getAvrPressure(){
        return avrPressure;
    }

    public Test(){

    }

    public Test(Device device) {
        setDevice(device);
    }

    public Test(Device device, float minTemperature, float maxTemperature, float avrTemperature, float minHumidity, float maxHumidity, float avrHumidity, float minPressure, float maxPressure, float avrPressure){
        setDevice(device);
        setMinTemperature(minTemperature);
        setMaxTemperature(maxTemperature);
        setAvrTemperature(avrTemperature);
        setMinHumidity(minHumidity);
        setMaxHumidity(maxHumidity);
        setAvrHumidity(avrHumidity);
        setMinPressure(minPressure);
        setMaxPressure(maxPressure);
        setAvrPressure(avrPressure);


    }
    public int getId() {
        return id;
    }

}
