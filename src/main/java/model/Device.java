package model;



import javax.persistence.*;
import  java.util.*;

@Entity
@Table(name = "devices", schema= "test chamber")

public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    public int getId() {
        return id;
    }
    @Id
    private String name;
    public void setName(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }
    @Column(name = "datasheet")
    private String datasheet;

    public void setDatasheet(String datasheet){
        this.datasheet = datasheet;
    }
    public String getDatasheet(){
        return datasheet;
    }

   @OneToMany(mappedBy = "device", cascade = CascadeType.ALL)

    private List tests;

    public Device(){

    }

    public Device(String title){
        setName(title);
    }

    public Device(String title, String datadheet){
        setName(title);
        setDatasheet(datadheet);
        tests = new ArrayList();
    }
    public void addTest(Test test){
        test.setDevice(this);
        tests.add(test);

    }

}
