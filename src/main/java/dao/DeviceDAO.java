package dao;

import model.*;
import java.sql.SQLException;

public interface DeviceDAO{
    public void addDevice(Device device) throws SQLException;
    public void  updateDevice(Device device) throws SQLException;
    public void  deleteDevice(Device device) throws SQLException;
    public Device findDeviceById(int id) throws SQLException;
    public Device findDeviceByName(String name) throws SQLException;

}

