package dao;

import model.Device;
import model.Test;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class TestDAOImpl implements TestDAO {

    public void addTest(Test test) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(test);
        tx1.commit();
        session.close();
    }
    public void updateTest(Test test) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(test);
        tx1.commit();
        session.close();
    }

    public void deleteTest(Test test) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(test);
        tx1.commit();
        session.close();

    }

    public Test findTestById(int id) throws SQLException {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Test.class, id);

    }

    public Collection getTestsByDevice(Device device) throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        List<Test> tests;

        int device_id = device.getId();
        Query query =  session.createQuery("From Test where device_id = :deviceId").setInteger("deviceId", device_id);
        tests = (List<Test>)query.list();
        tx1.commit();
        session.close();
        return tests;

    }
}
