package dao;

import model.*;

import java.sql.SQLException;
import java.util.Collection;

public interface TestDAO{
    public void addTest (Test test) throws SQLException;
    public void  updateTest(Test test) throws SQLException;
    public void  deleteTest(Test test) throws SQLException;
    public Test findTestById(int id) throws SQLException;
    public Collection  getTestsByDevice(Device device)throws SQLException;

}

