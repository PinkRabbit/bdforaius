package dao;

import dao.DeviceDAO;
import model.Device;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateSessionFactoryUtil;

import java.sql.SQLException;

public class DeviceDAOImpl implements DeviceDAO {
    public void addDevice (Device device) throws SQLException{
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.save(device);
        tx1.commit();
        session.close();
    }

    public void updateDevice(Device device) throws SQLException {

        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.update(device);
        tx1.commit();
        session.close();

    }

    public void deleteDevice(Device device)throws SQLException {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx1 = session.beginTransaction();
        session.delete(device);
        tx1.commit();
        session.close();

    }

    public Device findDeviceById(int id)throws SQLException{
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Device.class, id);
    }

    public Device findDeviceByName(String name) throws SQLException {
        return HibernateSessionFactoryUtil.getSessionFactory().openSession().get(Device.class, name);
    }
}
