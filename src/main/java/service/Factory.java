package service;

import dao.*;
import java.util.List;

public class Factory {

    private static DeviceDAO deviceDao = null;
    private static TestDAO testDao = null;

    private static Factory instance = null;

    public static synchronized Factory getInstance(){
        if (instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public DeviceDAO getDeviceDao(){
        if (deviceDao == null){
            deviceDao = new DeviceDAOImpl();
        }
        return deviceDao;
    }

    public TestDAO getTestDao(){
        if (testDao ==  null){
            testDao = new TestDAOImpl();
        }
        return testDao;
    }


}
