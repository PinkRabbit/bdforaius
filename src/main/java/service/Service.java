package service;

import model.*;

import java.sql.SQLException;
import java.util.Collection;

public class Service {
    public Service(){}
    public void addDevise(Device device)throws SQLException{
        Factory.getInstance().getDeviceDao().addDevice(device);
    }
    public Device findDevise(int id) throws SQLException {
        return Factory.getInstance().getDeviceDao().findDeviceById(id);
    }
    public Device findDevice(String name) throws SQLException{
        return Factory.getInstance().getDeviceDao().findDeviceByName(name);
    }
    public void deleteDevice(Device device) throws SQLException{
        Factory.getInstance().getDeviceDao().deleteDevice(device);
    }
    public void updateDevice(Device device) throws SQLException{
        Factory.getInstance().getDeviceDao().updateDevice(device);
    }
    public void addTest(Test test) throws SQLException{
        Factory.getInstance().getTestDao().addTest(test);
    }
    public void updateTest(Test test)throws SQLException{
        Factory.getInstance().getTestDao().updateTest(test);
    }
    public void deleteTest(Test test)throws SQLException{
        Factory.getInstance().getTestDao().deleteTest(test);
    }
    public Test findTest(int id) throws SQLException{
        return Factory.getInstance().getTestDao().findTestById(id);
    }

    public Collection  getTestByDevice(Device device) throws SQLException{
        return Factory.getInstance().getTestDao().getTestsByDevice(device);
    }

}
